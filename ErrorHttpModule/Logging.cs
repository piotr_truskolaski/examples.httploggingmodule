﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using System.IO;
using log4net.Config;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace NowOnline.Services.ErrorHandling
{
    /// <summary>
    /// Uses Log4NET to log messages through a Singleton. The configuration is taken from an embedded log4net.config file
    /// </summary>
    public class Logging
    {
        private static Logging instance;
        private static object instanceLock = new object();
        private ILog logger;

        private Logging()
        {
            Stream configStream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("NowOnline.Services.ErrorHandling.log4net.config");
            XmlConfigurator.Configure(configStream);
            logger = LogManager.GetLogger("NowOnline ErrorHandling HttpModule");
        }

        public static Logging Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (instanceLock)
                    {
                        if (instance == null)
                        {
                            instance = new Logging();
                        }
                    }
                }

                return instance;
            }
        }

        public void Error(string message, params string[] args)
        {
            logger.Error(string.Format(message, args));
        }
    }
}
